﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SECalcWPF.Data
{
	public class SEPlannerDatabase
	{
		private SQLiteConnection _Database;

		public SEPlannerDatabase()
		{
			_Database = new SQLiteConnection("SEPlannerData.db3");
			_Database.CreateTable<Ore>();
			_Database.CreateTable<Ingot>();
		}
	}
}
