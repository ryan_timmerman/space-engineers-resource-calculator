﻿using PropertyChanged;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SECalcWPF.Data
{
	public class Ore
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }
		public string Name { get; set; }
		public double MassKg { get; set; }
		public double VolumeL { get; set; }
	}
}
