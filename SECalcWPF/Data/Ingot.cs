﻿using PropertyChanged;
using SQLite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SECalcWPF.Data
{
	public class Ingot
	{
		[PrimaryKey, AutoIncrement]
		public int ID { get; set; }

		public int OreID { get; set; }

		public string Name { get; set; }
		
		public double RecoveryRatio { get; set; }

	}
}
