﻿using PropertyChanged;
using SECalcWPF.Data;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SECalcWPF.ViewModels
{
	[ImplementPropertyChanged]
	public class MainViewModel
	{
		public ObservableCollection<Ore> Ores;
		public ObservableCollection<Ingot> Ingots;
	}
}
