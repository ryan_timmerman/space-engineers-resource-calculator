﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SECalcWPF.ViewModels
{
	public class Command : ICommand
	{
		public event EventHandler CanExecuteChanged;

		private Action _Action;
		private Func<bool> _CanExecute;

		public Command(Action action) : this(action, () => true)
		{
		}

		public Command(Action action, Func<bool> canExecute)
		{
			_Action = action;
			_CanExecute = canExecute;
		}

		public bool CanExecute(object parameter)
		{
			return _CanExecute?.Invoke() ?? false;
		}

		public void Execute(object parameter)
		{
			if(CanExecute(parameter))
			{
				_Action?.Invoke();
			}
		}
	}

	public class Command<T> : ICommand
	{
		public event EventHandler CanExecuteChanged;

		private Action<T> _Action;
		private Predicate<T> _CanExecute;

		public Command(Action<T> action) : this(action, (x) => true)
		{

		}

		public Command(Action<T> action, Predicate<T> canExecute)
		{
			_Action = action;
			_CanExecute = canExecute;
		}

		public bool CanExecute(object parameter)
		{
			return _CanExecute((T)parameter);
		}

		public void Execute(object parameter)
		{
			if(CanExecute(parameter))
			{
				_Action?.Invoke((T)parameter);
			}
		}
	}
}
