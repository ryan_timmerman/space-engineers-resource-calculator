﻿using PropertyChanged;
using SECalcWPF.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SECalcWPF.ViewModels
{
	public class OreVM : ViewModelBase
	{
		private Ore _Ore;
		public OreVM(Ore ore)
		{
			_Ore = ore;
		}

		public string Name
		{
			get
			{
				return _Ore.Name;
			}
			set
			{
				_Ore.Name = value;
				OnPropertyChanged(nameof(Name));
			}
		}

		public double MassPerUnit
		{
			get
			{
				return _Ore.MassKg;
			}
			set
			{
				_Ore.MassKg = value;
				OnPropertyChanged(nameof(MassPerUnit));
			}
		}

		public double VolumePerUnit
		{
			get
			{
				return _Ore.VolumeL;
			}
			set
			{
				_Ore.VolumeL = value;
				OnPropertyChanged(nameof(VolumePerUnit));
			}
		}
	}
}
