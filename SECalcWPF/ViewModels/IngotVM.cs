﻿using SECalcWPF.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SECalcWPF.ViewModels
{
	public class IngotVM : ViewModelBase
	{
		private Ingot _Ingot;
		private Ore _Ore;

		public IngotVM(Ingot ingot, Ore ore)
		{
			_Ingot = ingot;
			_Ore = ore;
		}
	}
}
